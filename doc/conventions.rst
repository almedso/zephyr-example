Applied Zephyr Project Conventions
==================================

C++
---

see https://docs.zephyrproject.org/1.12.0/kernel/other/cxx_support.html

Only possible for applications.
Configure in *prj.conf*

.. code::

   CONFIG_CPLUSPLUS=y

Do not use:

* new, delete
* Exceptions
* RTTI
* Static global object destruction

Logging
-------

see https://docs.zephyrproject.org/latest/subsystems/logging/index.html


Run all applications with (sys) logging module switched.
Do it via settings in prj.conf

.. code::

   CONFIG_SYS_LOG=y
   CONFIG_SYS_LOG_DEFAULT_LEVEL=4


Debug level are as follow:

* 1 - ERR (error)
* 2 - WRN (warning)
* 3 - INF (info)
* 4 - DBG (debug)


Development builds should use Log level 4 (highest including Debug)
Production builds should only be set to level error.

Log Level
---------

Error:

* whenever a reset of entire mcu oder a subsystem is required
* whenever a communication line gets broken that is assumed not to break

Warning:

* whenever a subsystem delivers inconsistent values
* whenever values are received that are out of range
* whenever a communication line (that can break) gets broken that and can be reestablished

Info:

* all intenal state changes
* all incoming requests
* all activities (start and end)

Debug:

* all The rest


