How To Do's
===========


Sign an image
-------------

Use the image tool as shown described .. at

https://mcuboot.com/mcuboot/readme-zephyr.html


.. code-block :: console

   MCUBOOT_DIR=$HOME/devel/mcuboot  # or somewhere else
   $MCUBOOT_DIR/scripts/imgtool.py sign \
     --key $MCUBOOT_DIR/root-rsa-2048.pem \
     --align 8 --version 1.2 \
     --slot-size 0xa000 \
     --header-size 0x200 \
     zephyr.bin signed-app.bin

The slot size must be bigger than the application.

Device Firmware Update via USB
------------------------------

DFU 1.1 protocol is part of USB specification.


use the DFU tool *dfu-util* on linux like:


.. code-block :: bash

    $ sudo dfu-util --alt 1 --download signed-app.bin 
    dfu-util 0.9

    Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
    Copyright 2010-2016 Tormod Volden and Stefan Schmidt
    This program is Free Software and has ABSOLUTELY NO WARRANTY
    Please report bugs to http://sourceforge.net/p/dfu-util/tickets/

    dfu-util: Invalid DFU suffix signature
    dfu-util: A valid DFU suffix will be required in a future dfu-util release!!!
    Opening DFU capable USB device...
    ID 2fe3:0100
    Run-time device DFU version 0110
    Claiming USB DFU Runtime Interface...
    Determining device status: state = appIDLE, status = 0
    Device really in Runtime Mode, send DFU detach request...
    Resetting USB...
    Opening DFU USB Device...
    Claiming USB DFU Interface...
    Setting Alternate Setting #1 ...
    Determining device status: state = dfuIDLE, status = 0
    dfuIDLE, continuing
    DFU mode device DFU version 0110
    Device returned transfer size 64
    Copying data from PC to DFU device
    Download	[=========================] 100%        12908 bytes
    Download done.
    state(2) = dfuIDLE, status(0) = No error condition is present
    Done!


the console output on the device is like (if logging is switch on for the respective apps).

.. code-block :: console

    ***** Booting Zephyr OS zephyr-v1.13.0-2349-g3ac7c12b4f *****
    [MCUBOOT] [INF] main: Starting bootloader
    [MCUBOOT] [INF] boot_status_source: Image 0: magic=good, copy_done=0x1, image_ok=0x3
    [MCUBOOT] [INF] boot_status_source: Scratch: magic=unset, copy_done=0x18, image_ok=0x3
    [MCUBOOT] [INF] boot_status_source: Boot source: none
    [MCUBOOT] [INF] boot_swap_type: Swap type: revert
    [MCUBOOT] [INF] main: Bootloader chainload address offset: 0x20000
    ***** Booting Zephyr OS zephyr-v1.13.0-2349-g3ac7c12b4f *****
    [00:00:00.000,000] <inf> main.main: This device supports USB DFU class.

    ***** Booting Zephyr OS zephyr-v1.13.0-2349-g3ac7c12b4f *****
    [MCUBOOT] [INF] main: Starting bootloader
    [MCUBOOT] [INF] boot_status_source: Image 0: magic=good, copy_done=0x1, image_ok=0x1
    [MCUBOOT] [INF] boot_status_source: Scratch: magic=unset, copy_done=0x18, image_ok=0x3
    [MCUBOOT] [INF] boot_status_source: Boot source: none
    [MCUBOOT] [INF] boot_swap_type: Swap type: test
    [MCUBOOT] [INF] main: Bootloader chainload address offset: 0x20000
    [MCUBOOT] [INF] main: Jumping to the first image slot�0***** Booting Zephyr OS zephyr-v1.13.0-2349-g3ac*
    Hello World! frdm_k64f


See also the dfu example documentation of zephyr:

https://docs.zephyrproject.org/latest/samples/subsys/usb/dfu/README.html


LPC-Link2
---------

NXP provides for some of their eval/development boards LPC-Link2 in order to 

* Flash / erase
* debug

via Segger "lite" j-link protocol.
Segger "lite" is free of charge. All Segger tools are available for Linux/Mac/Windows.

Default delivery of development boards like LPCXpresso54114 come along with an outdated
LPC-Link2 firmware. The tool LPCScrypt is around (and available on Linux/Mac/Windows to
prepare LPC-Link2 to talk J-Link protocol.

get the tool + docu + tutorial video at:

https://www.nxp.com/support/developer-resources/software-development-tools/lpc-developer-resources-/lpc-microcontroller-utilities/lpcscrypt-v2.0.0:LPCSCRYPT

The tool comes as a debian package *lpcscrypt*

.. code :: (bash)

   $ dpkg -l lpcscrypt  # check if it is installed
   $ dpkg -L lpcscrypt  # lists content

.. note ::

   The install base on Windows is *c:\nxp* and on Linux */usr/local*.


Update the firmware by the following steps:

.. code :: (bash)

   $ sudo /usr/local/lpcscrypt-2.0.0_831/scripts/boot_lpcscrypt

boots and updates firmware and makes a */dev/ttyACM?* device visible to the linux host (or anything similar like a VCOM device on Windows)
Still, it does not operate as a J-link USB device yet.

In order to enable running J-Link tools ("lite") run:

.. code :: (bash)

   $ sudo /usr/local/lpcscrypt-2.0.0_831/scripts/program_JLINK

And you are all set after a full blown power cycling. Potentially this needs to be done on Windows and or Linux.
Also, the Jumper (called LPC Link, located  between both USB connectors) needs to be unset/open for programming.

See also
https://www.segger.com/products/debug-probes/j-link/models/other-j-links/lpc-link-2/








