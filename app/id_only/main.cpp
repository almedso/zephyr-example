/*
 *  Copyright (c) 2018 almedso GmbH
 *  SPDX-License-Identifier: MIT
 */

#include <zephyr.h>
#include <stdio.h>

#include "identification.h"

void main(void)
{
  print_app_identification();
  while (1) {
  }
}
