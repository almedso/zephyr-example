FRDM_KL25Z
==========

Flashing the Board
******************

* connect usb to OpenSDA.
* This will mount an USB storage drive
* copy the **bin** file (located as zephyr/zephyr.bin) to the root directory of the mounted drive


Connecting to the terminal
**************************

while connecting the USB OpenSDA cable another device */dev/ttyACM1*
shows up. - This is the device console. use it with 115200 Boud and 8N1 you will get console output.

... and watch the console.

