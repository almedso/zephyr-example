LPCXpresso54114
===============

The LPCXpresso54114 is a development board of the NXP LPC5xxxx line with at least CortexM4.
It comes along with LPC-Link2 add-on for flashing and easy debugging based on LPC4322 chip.
Among others it supports Zephyr OS out of the box.


References
----------

* https://docs.zephyrproject.org/latest/boards/arm/lpcxpresso54114/doc/lpcxpresso54114.html (zephyr)
* https://www.mouser.com/ds/2/302/UM10973-953210.pdf (user guide - mouser only)
* 