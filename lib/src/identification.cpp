/*
 *  Copyright (c) 2018 almedso GmbH
 *  SPDX-License-Identifier: MIT
 */

#include <stdio.h>

#include "identification.h"
#include "generated_identifiers.h"


const char kApplication[] = APPLICATION;
const char kVersion[] = VERSION;
const char kFullName[] = APPLICATION "@" VERSION;

/* it is supposed that this is overwritten by querying some
   hardware e.g. i2c eeproms for hardware identification */
#define HARDWARE_IDENTIFIER_SIZE 50
char hardware_identifier[HARDWARE_IDENTIFIER_SIZE] = BOARD "@" BOARD_REVISION;

void print_app_identification(void)
{
  printf("***** and     Application: %s ******\n", kFullName);
  printf("***** on      Hardware ID: %s ******\n", hardware_identifier );
}

