/**
 * Copyright (c) 2018 almedso GmbH
 * SPDX-License-Identifier: MIT
 */
#pragma once

/** The name of the application */
extern const char kApplication[];
/** The version of the application */
extern const char kVersion[];
/** The full Name is <application>@<version> */
extern const char kFullName[];
/** Something identifying the hardware uniquely like board name and version */
extern char hardware_identifier[];

/**
 * print application and hardware identification on console
 */
void print_app_identification(void);
