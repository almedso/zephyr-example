Zephyr Applications
###################

Overview
********

This is a 
multi application git repository containing zephyr based applications
on various boards including 
related documentation in *sphinx* format (rst) (i.e. this readme is part of it)


Building the documentation
**************************

.. code-block :: shell

   $ docker run --rm --volume $PWD:/documents/ --user $(id -u) almedso/sphinx-doc-builder


The output is placed at *$PWD/build/html/doc/index.html* as well as to *$PWD/build/pdf*.



Building and Running
********************

Zephyr project facilitates **cmake**. in a special way.

see: https://docs.zephyrproject.org/1.13.0/application/application.html#cmake-details

By convention all output should be placed below a *build* folder into a directory like the
*application_name-board_name*  (note the underscore in the name).
this will nicely simplify building.

In the home folder of the project do:

.. code-block :: shell

   $ # source the zephyr enviornment e.g.
   $ source ~/extended/zephyr/zephyr-env.sh  # or wherever it is installed
   $ # and build
   $ mkdir build/id_only-frdm_k64f ; cd build/id_only-frdm_k64f
   $ cmake -GNinja -DBOARD=frdm_k64f -DAPP=id_only -DVERSION=$(git describe) ..
   $ ninja


.. note ::

   differnent for the zephyr getting started guide, we provide the name of the
   application as well.

